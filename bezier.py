#!/usr/bin/env python3

### Function to determine the interpolated point, based on a degree 2 spline with 3 controllpoints ###
### Controlvector = (0,0,0,1,1,1) -> Corresponds to a three point bezier curve ###
def point(A,B,C,x):
    return A * (1-x)**2 + B * 2 * (1-x)*x + C * x**2

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

A = np.array([1,1])
B = np.array([2,3])
C = np.array([3,2])

vector_AB = B-A
vector_BC = C-B

if __name__ == "__main__":
    fig = plt.figure()
    ax = fig.add_subplot(121)
    plt.subplots_adjust(bottom=0.25)

    x_points = [A[0],B[0],C[0]]
    y_points = [A[1],B[1],C[1]]

    construction_x = [A[0],A[0]+0.25*vector_AB[0],B[0]+0.25*vector_BC[0],C[0]]
    construction_y = [A[1],A[1]+0.25*vector_AB[1],B[1]+0.25*vector_BC[1],C[1]]

    plt.plot(x_points,y_points,"-b")
    interpolated_point = point(A,B,C,0.25)
    l_1, = plt.plot(interpolated_point[0],interpolated_point[1],"ro")
    l_2, = plt.plot(construction_x,construction_y,"-r")
    ax = plt.axes([0.15, 0.1, 0.65, 0.03])

    slider_x = Slider(ax,'x',0,1,valinit=0.25)

    def update(value):
        l_1.set_xdata(point(A,B,C,value)[0])
        l_1.set_ydata(point(A,B,C,value)[1])
        l_2.set_xdata([A[0],A[0]+value*vector_AB[0],B[0]+value*vector_BC[0],C[0]])
        l_2.set_ydata([A[1],A[1]+value*vector_AB[1],B[1]+value*vector_BC[1],C[1]])

        fig.canvas.draw_idle()
    
    slider_x.on_changed(update)

    fig.add_subplot(122)

    x = np.linspace(0,1)
    b_1 = (1-x)**2
    b_2 = 2*(1-x)*x
    b_3 = x**2

    plt.plot(x,b_1,label="$B_1$")
    plt.plot(x,b_2,label="$B_2$")
    plt.plot(x,b_3,label="$B_3$")
    plt.legend()


    plt.show()
    

